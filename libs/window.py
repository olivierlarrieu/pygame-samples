import pygame
from pygame.locals import *

class PygameWindow():
    
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode([self.width,self.height])
        pygame.init()
