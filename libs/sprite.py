import pygame
from pygame.sprite import Sprite


class MySprite(Sprite):
    
    def __init__(self, window, pos_x, pos_y, image, *groups):
        self.window = window
        self.image = pygame.image.load(image)
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.rect = self.image.get_rect()
        self.rect.move(self.pos_x, self.pos_y)
        return super().__init__(*groups)

    def update(self, *args):
        #self.pos_x += 0.3
        # self.pos_y += 0.3
        self.window.screen.blit(self.image, (self.pos_x, self.pos_y))
