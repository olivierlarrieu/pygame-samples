import pygame


clock = pygame.time.Clock()

def pygame_update(func):
    def wrapper(window, *args, **kwargs):
        window.screen.fill((0, 0, 0))
        func(window, *args, **kwargs)
        pygame.display.update()
        clock.tick(60)
    return wrapper
