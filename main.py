from pygame.sprite import Group
from libs.window import PygameWindow
from libs.sprite import MySprite
from libs.decorators import pygame_update


def create_flags():
    flag_group = Group()
    for i in range(10):
        flag_group.add(MySprite(window, i*50, 50, 'assets/image.png'))

    for i in range(3):
        flag_group.add(MySprite(window, i*50, 150, 'assets/image.png'))

    for i in range(20):
        flag_group.add(MySprite(window, i*50, 350, 'assets/image.png'))

    return flag_group

def create_player():

    class Player(MySprite):
        def update(self, *args):
            self.pos_x += 0.2
            self.rect.move(self.pos_x, self.pos_y)
            self.window.screen.blit(self.image, (self.pos_x, self.pos_y))

    player_group = Group()
    player_group.add(Player(window, 50, 250, 'assets/image.png'))
    return player_group


@pygame_update
def main_game(window, flag_group, player):
    flag_group.update()
    player.update()

if __name__ == "__main__":
    window = PygameWindow(width=700, height=400)
    flag_group = create_flags()
    player = create_player()

    while True:
        main_game(window, flag_group, player)
